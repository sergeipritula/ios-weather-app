//
//  WeatherData.swift
//  TestTask
//
//  Created by Sergey on 11/20/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation

class WeatherDataParameters {
    var temperature : Double = 0
    var pressure : Double = 0
    var humidity : Double = 0
    var windSpeed : Double = 0
    var windDegree : Double = 0
    var cityId : Int = 0
    var iconName: String = ""
    var title: String = ""
    var updateDate: Date = Date()
    
    init(dictionary: NSDictionary, city : City) {
        self.getObject(fromDictionary: dictionary, city: city)
    }
    
    init(data: Data?, city : City) {
        self.getObject(fromData: data, city: city)
    }
    
    func getObject(fromData: Data?, city: City) {
        do {
            self.cityId = city.value(forKey: "Id") as! Int;
            self.updateDate = Date()
            if let data = fromData,
                let json = try JSONSerialization.jsonObject(with: data) as? NSDictionary,
                let _ = json["name"] as? String {
                if let main = json["main"] as? NSDictionary {
                    if let temp = main["temp"] as? Double {
                        self.temperature = temp
                    }
                    if let pressure = main["pressure"] as? Double {
                        self.pressure = pressure
                    }
                    if let humidity = main["humidity"] as? Double {
                        self.humidity = humidity
                    }
                }
                
                if let weather = json["weather"] as? NSArray {
                    if let weatherData = weather[0] as? NSDictionary {
                        if let iconName = weatherData["icon"] as? String {
                            self.iconName = iconName
                        }
                        if let description = weatherData["description"] as? String {
                            self.title = description
                        }
                    }
                }
                
                if let wind = json["wind"] as? NSDictionary {
                    if let speed = wind["speed"] as? Double {
                        self.windSpeed = speed
                    }
                    if let degree = wind["deg"] as? Double{
                        self.windDegree = degree
                    }
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
    }
    
    func getObject(fromDictionary: NSDictionary, city: City) {
        if let main = fromDictionary["main"] as? NSDictionary {
            self.cityId = Int(city.id)
            if let temp = main["temp"] as? Double {
                self.temperature = temp
            }
            if let pressure = main["pressure"] as? Double {
                self.pressure = pressure
            }
            if let humidity = main["humidity"] as? Double {
                self.humidity = humidity
            }
        }
        
        if let weather = fromDictionary["weather"] as? NSArray {
            if let weatherData = weather[0] as? NSDictionary {
                if let iconName = weatherData["icon"] as? String {
                    self.iconName = iconName
                }
                if let description = weatherData["description"] as? String {
                    self.title = description
                }
            }
        }
        
        if let wind = fromDictionary["wind"] as? NSDictionary {
            if let speed = wind["speed"] as? Double {
                self.windSpeed = speed
            }
            if let degree = wind["deg"] as? Double{
                self.windDegree = degree
            }
        }
        
        if let date = fromDictionary["dt_txt"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.updateDate = dateFormatter.date(from: date)!
        }
    }
}
