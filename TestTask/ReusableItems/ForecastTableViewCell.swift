//
//  ForecastTableViewCell.swift
//  TestTask
//
//  Created by Sergey on 11/21/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation
import UIKit

class ForecastTableViewCell : UITableViewCell {
    
    @IBOutlet weak var forecastConditionImage: UIImageView!
    @IBOutlet weak var forecastTimeCellLabel: UILabel!
    @IBOutlet weak var windDegreeCellLabel: UILabel!
    @IBOutlet weak var windSpeedCellLabel: UILabel!
    @IBOutlet weak var humidityCellLabel: UILabel!
    @IBOutlet weak var pressureCellLabel: UILabel!
    @IBOutlet weak var temperatureCellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
