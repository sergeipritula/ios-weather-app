# README #

iOS weather application
The application consists of 3 screens: master, detail and search.

Master screen displays the list of cities. The user may filter the list of cities, add new, delete and select cities.
Filtering is by the name of the city, entered in the search field at the top of screen. For filtering use UISearchController.

Tapping on the “add” button transfers the user to the search screen. Deletion of the city is made by swipe gesture (native feature). Selection of the city transfers the user to the detail screen.

Search screen allows user to search cities by name. By tapping on the city user adds selected city to the master screen.

Detail screen displays current weather (temperature, humidity, pressure, wind, cloud...) and the forecast in the selected city. Use icons for displaying different weather conditions.
Once you get to the details screen, send weather request for the selected city. While the request is in progress, display the latest saved weather data. After receiving response update data.

For getting weather data used OpenWeatherApi.