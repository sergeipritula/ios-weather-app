//
//  ViewController.swift
//  TestTask
//
//  Created by Sergey on 11/16/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import UIKit
import CoreData
import GooglePlaces

class MasterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func runSearchCityController(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        
        autocompleteController.delegate = self
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    private var cities : [City] = []
    private var filteredCities = [String]()
    private var cityToDetailView = City()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation  = false
        searchController.searchBar.placeholder = "Filter..."
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        self.cities = CitiesService.sharedInstance.getCities()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredCities.count
        }
        
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReusedIdentifier")!
        let city = cities[indexPath.row]
        
        if isFiltering() {
            cell.textLabel?.text = filteredCities[indexPath.row]
        } else {
            cell.textLabel?.text = city.value(forKeyPath: "cityName") as? String
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let deleted = CitiesService.sharedInstance.delete(city: cities[indexPath.row])
        print("Delete object : \(deleted)")
        self.reloadData();
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        let citiesList = CitiesService.sharedInstance.getCities().map { $0.value(forKey: "cityName") } as! [String]
        filteredCities = citiesList.filter({ (cityName : String?) -> Bool in
            return (cityName?.lowercased().contains(searchText.lowercased()))!
        })
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func reloadData() {
        cities = CitiesService.sharedInstance.getCities()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityToDetailView = cities[indexPath.row]
        //self.performSegue(withIdentifier: "citySegueIdentifier", sender: self)
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "detailViewController") as! DetailViewController
        destination.configure(withConfig: cityToDetailView)
        navigationController?.pushViewController(destination, animated: true)
    }
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "citySegueIdentifier", let destination = segue.destination as? DetailViewController {
            destination.city = cityToDetailView
        }
    }*/
}

extension MasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension MasterViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let added = CitiesService.sharedInstance.add(name: place.name)
        print("Added object : \(added)")
        reloadData();
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
