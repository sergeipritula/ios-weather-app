//
//  WeatherDataService.swift
//  TestTask
//
//  Created by Sergey on 11/20/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation
//
//  WeatherDataService.swift
//  TestTask
//
//  Created by Sergey on 11/17/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class WeatherDataService {
    
    static let sharedInstance = WeatherDataService()
    private let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let managedContext : NSManagedObjectContext
    
    private init() {
        managedContext = (appDelegate?.persistentContainer.viewContext)!
    }
    
    func getWeatherObjectByCityId(city: City) -> WeatherData? {
        let weatherDataList = getWeatherDataList()
        
        if (weatherDataList.contains(where: {
            return $0.cityId == city.id
        })) {
            return weatherDataList.first(where : { $0.cityId == city.id })
        } else {
            return nil
        }
    }
    
    func updateWeatherDataObject(city: City, weatherDataObject: WeatherDataParameters) {
        if let weatherManagedObject = getWeatherObjectByCityId(city: city) {
            weatherManagedObject.setValue(weatherDataObject.cityId, forKeyPath: "cityId")
            weatherManagedObject.setValue(weatherDataObject.temperature, forKeyPath: "temperature")
            weatherManagedObject.setValue(weatherDataObject.pressure, forKeyPath: "pressure")
            weatherManagedObject.setValue(weatherDataObject.humidity, forKeyPath: "humidity")
            weatherManagedObject.setValue(weatherDataObject.windSpeed, forKeyPath: "windSpeed")
            weatherManagedObject.setValue(weatherDataObject.windDegree, forKeyPath: "windDegree")
            weatherManagedObject.setValue(weatherDataObject.updateDate, forKeyPath: "updateDate")
            weatherManagedObject.setValue(weatherDataObject.iconName, forKeyPath: "iconName")
            weatherManagedObject.setValue(weatherDataObject.title, forKeyPath: "title")
        }
        appDelegate?.saveContext()
    }
    
    func getWeatherDataList() -> [WeatherData] {
        let fetchRequest = NSFetchRequest<WeatherData>(entityName: "WeatherData")
        var weatherDataList : [WeatherData] = []
        
        do {
            weatherDataList = (try managedContext.fetch(fetchRequest))
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return weatherDataList
    }
    
    func add(weatherDataObject : WeatherDataParameters) -> WeatherData {
        let entity = NSEntityDescription.entity(forEntityName: "WeatherData", in: managedContext)!
        let weatherManagedObject = WeatherData(entity: entity, insertInto: managedContext)
        weatherManagedObject.setValue(weatherDataObject.cityId, forKeyPath: "cityId")
        weatherManagedObject.setValue(weatherDataObject.temperature, forKeyPath: "temperature")
        weatherManagedObject.setValue(weatherDataObject.pressure, forKeyPath: "pressure")
        weatherManagedObject.setValue(weatherDataObject.humidity, forKeyPath: "humidity")
        weatherManagedObject.setValue(weatherDataObject.windSpeed, forKeyPath: "windSpeed")
        weatherManagedObject.setValue(weatherDataObject.windDegree, forKeyPath: "windDegree")
        weatherManagedObject.setValue(weatherDataObject.updateDate, forKeyPath: "updateDate")
        weatherManagedObject.setValue(weatherDataObject.iconName, forKeyPath: "iconName")
        weatherManagedObject.setValue(weatherDataObject.title, forKeyPath: "title")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        return weatherManagedObject
    }
    
    func delete(city: City) {
        if let weatherDataObject = getWeatherObjectByCityId(city: city) {
            managedContext.delete(weatherDataObject)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
