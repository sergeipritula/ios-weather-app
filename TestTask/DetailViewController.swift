//
//  DetailViewController.swift
//  TestTask
//
//  Created by Sergey on 11/17/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    private var Forecast = [WeatherDataParameters]()
    var city = City()
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDegreeLabel: UILabel!
    @IBOutlet weak var updateTimeLabel: UILabel!
    @IBOutlet weak var iconConditionView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forecastTableView: UITableView!
    
    func configure(withConfig city : City) {
        self.city = city
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        cityLabel.text = self.city.cityName
        forecastTableView.delegate = self
        forecastTableView.dataSource = self
        
        if let weatherManagedObject = WeatherDataService.sharedInstance.getWeatherObjectByCityId(city: self.city) {
            self.updateUI(data: weatherManagedObject)
        }
        
        let weatherGetterObject = WeatherGetterService()
        weatherGetterObject.getWeather(cityObject: self.city, completion:  { (weatherDataObject) in
            DispatchQueue.main.async {
                if let currentWeather = weatherDataObject {
                    self.updateUI(data : currentWeather)
                    
                    if  let weatherManagedObject = WeatherDataService.sharedInstance.getWeatherObjectByCityId(city: self.city) {
                        WeatherDataService.sharedInstance.updateWeatherDataObject(city: self.city, weatherDataObject: currentWeather)
                    } else {
                        let added = WeatherDataService.sharedInstance.add(weatherDataObject: currentWeather)
                        print("Added weather info : \(added)")
                    }
                } else {
                    self.showErrorAlert(condition: "weather")
                }
            }
        })
        
        weatherGetterObject.getForecast(cityObject: self.city, completion: { (weatherObjectsList) in
            DispatchQueue.main.async {
                if let currentForecastList = weatherObjectsList {
                    if currentForecastList.count > 0 {
                        self.Forecast = currentForecastList
                        self.forecastTableView.reloadData()
                    } else {
                        self.showErrorAlert(condition: "forecast")
                    }
                } else {
                    self.showErrorAlert(condition: "forecast")
                }
            }
        })
    }
    
    func updateUI(data : WeatherDataParameters) {
        temperatureLabel.text = String(describing: data.temperature)
        pressureLabel.text = String(describing: data.pressure)
        humidityLabel.text = String(describing: data.humidity)
        windSpeedLabel.text = String(describing: data.windSpeed)
        windDegreeLabel.text = String(describing: data.windDegree)
        descriptionLabel.text = String(describing: data.title)
        iconConditionView.image = UIImage(named: data.iconName)
        updateTimeLabel.text  = self.getFormatedDateString(updateDate: data.updateDate)
        print("cityID = \(data.cityId)")
    }
    
    func updateUI(data : WeatherData) {
        temperatureLabel.text = String(describing: data.temperature)
        pressureLabel.text = String(describing: data.pressure)
        humidityLabel.text = String(describing: data.humidity)
        windSpeedLabel.text = String(describing: data.windSpeed)
        windDegreeLabel.text = String(describing: data.windDegree)
        descriptionLabel.text = data.value(forKey: "title") as? String
        iconConditionView.image = UIImage(named: data.iconName!)
        updateTimeLabel.text  = self.getFormatedDateString(updateDate: (data.value(forKey: "updateDate") as? Date)!)
        
        print("cityID = \(data.cityId)")
    }
    
    func showErrorAlert(condition: String) {
        let alert = UIAlertController(title: "Error", message: "Couldn't load \(condition) data", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Forecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = forecastTableView.dequeueReusableCell(withIdentifier: "reusedForecastCell") as? ForecastTableViewCell {
            let forecast = Forecast[indexPath.row]
            
            cell.temperatureCellLabel.text = String(describing: forecast.temperature)
            cell.pressureCellLabel.text = String(describing: forecast.pressure)
            cell.humidityCellLabel.text = String(describing: forecast.humidity)
            cell.windSpeedCellLabel.text = String(describing: forecast.windSpeed)
            cell.windDegreeCellLabel.text = String(describing: forecast.windDegree)
            cell.forecastTimeCellLabel.text = getFormatedDateString(updateDate: forecast.updateDate)
            cell.forecastConditionImage.image = UIImage(named: forecast.iconName)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func getFormatedDateString(updateDate: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: updateDate)
    }
    
}
