//
//  WeatherDataService.swift
//  TestTask
//
//  Created by Sergey on 11/17/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CitiesService {
    
    static let sharedInstance = CitiesService()
    private let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let managedContext : NSManagedObjectContext
    
    private init() {
        managedContext = (appDelegate?.persistentContainer.viewContext)!
    }
    
    func getCities() -> [City] {
        let fetchRequest = NSFetchRequest<City>(entityName: "City")
        var cities : [City] = []
        
        do {
            cities = (try managedContext.fetch(fetchRequest))
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return cities
    }
    
    private func generateId() -> Int {
        if (getCities().count > 0) {
            return (Int(getCities().last!.id + 1))
        }
        
        return 1
    }
    
    func add(name: String) -> City {
        let entity = NSEntityDescription.entity(forEntityName: "City", in: managedContext)!
        let city = City(entity: entity, insertInto: managedContext)
        city.setValue(name, forKeyPath: "cityName")
        city.setValue(self.generateId(), forKeyPath: "id")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        return city
    }
    
    func delete(city: City) -> City {
        managedContext.delete(city)
        WeatherDataService.sharedInstance.delete(city: city)
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        return city
    }
    
}
