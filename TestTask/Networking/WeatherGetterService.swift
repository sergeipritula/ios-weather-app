//
//  WeatherGetRequests.swift
//  TestTask
//
//  Created by Sergey on 11/18/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

import Foundation
import CoreData

class WeatherGetterService {
    func getWeather(cityObject: City, completion: @escaping (WeatherDataParameters?) -> ()) {
        let openWeatherMapBaseWeatherURL = Bundle.main.object(forInfoDictionaryKey: "OpenWeatherMapWeaherBaseURL") as! String
        let openWeatherMapAPIKey = Bundle.main.object(forInfoDictionaryKey: "OpenWeatherMapApiKey") as! String
        
        let cityName = cityObject.value(forKeyPath: "cityName") as? String
        let urlStr = openWeatherMapBaseWeatherURL + "/?q=" + cityName! + "&APPID=\(openWeatherMapAPIKey)"
        let url = URL(string: urlStr)
        let session = URLSession.shared
        
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                
                if (error == nil) {
                    let weatherDataObject = WeatherDataParameters(data: data, city: cityObject);
                    completion(weatherDataObject)
                } else {
                    print("Error getting response : \(String(describing: error))")
                    completion(nil)
                }
            })
            task.resume()
        }
    }
    
    func parseForecastResponse(data: Data?, city : City) -> [WeatherDataParameters] {
        var weatherDataList = [WeatherDataParameters]()
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? NSDictionary {
                if let  dataCount = json["cnt"] as? Int {
                    if let dataList = json["list"] as? NSArray {
                        
                        for i in 0...dataCount - 1 {
                            if let weatherData = dataList[i] as? NSDictionary {
                                let forecastDataObject = WeatherDataParameters(dictionary: weatherData, city: city)
                                weatherDataList.append(forecastDataObject)
                            }
                        }
                    }
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return weatherDataList
    }
    
    func getForecast(cityObject: City, completion: @escaping ([WeatherDataParameters]?) -> ()) {
        let openWeatherMapBaseForecastURL = Bundle.main.object(forInfoDictionaryKey: "OpenWeatherMapForecastBaseURL") as! String
        let openWeatherMapAPIKey = Bundle.main.object(forInfoDictionaryKey: "OpenWeatherMapApiKey") as! String
        
        let cityName = cityObject.value(forKeyPath: "cityName") as? String
        let urlStr = openWeatherMapBaseForecastURL + "/?q=" + cityName! + "&APPID=\(openWeatherMapAPIKey)"
        let url = URL(string: urlStr)
        let session = URLSession.shared
        
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                if (error == nil) {
                    completion(self.parseForecastResponse(data: data, city: cityObject))
                } else {
                    completion(nil)
                }
            })
            task.resume()
        }
    }
}
